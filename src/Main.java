import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        // TODO: Tính cạnh góc vuông

        // dữ liệu nhập vào : 2 cạnh góc vuông
        Scanner sc1 = new Scanner(System.in);
        System.out.print("Cạnh góc vuông thứ 1: ");
        double canhGocVuongThuNhat = sc1.nextDouble();
        System.out.print("Cạnh góc vuông thứ 2: ");
        double canhGocVuongThuHai = sc1.nextDouble();

        // quá trình xử lý : tính cạnh huyền khi đã biết trước 2 cạnh góc vuông
        if(canhGocVuongThuNhat > 0 && canhGocVuongThuHai > 0){
            double ketQuaBaiMot = Math.sqrt((canhGocVuongThuNhat * canhGocVuongThuNhat) + (canhGocVuongThuHai * canhGocVuongThuHai));
            // xuất kết quả
            System.out.println("Cạnh huyền: " + ketQuaBaiMot);
        }else {
            System.out.println("Cạnh góc vuông không được âm");
        }

        // TODO: Tính tổng số có 2 chữ số
        Scanner sc2 = new Scanner(System.in);
        System.out.print("Nhập số có 2 chữ số: ");
        if (sc2.hasNextInt()) {
            int soCoHaiChuSo = sc2.nextInt();
            if (soCoHaiChuSo >= 10 && soCoHaiChuSo <= 99) {
                int hangChuc = soCoHaiChuSo / 10;
                int hangDonVi = soCoHaiChuSo % 10;
                int ketQuaBai2 = hangChuc + hangDonVi;
                System.out.println("Tổng 2 chữ số là: " + ketQuaBai2);
            } else {
                System.out.println("Số không hợp lệ. Vui lòng nhập số có 2 chữ số.");
            }
        } else {
            System.out.println("Số nhập vào không phải là số nguyên.");
        }

        // TODO: Đổi USD sang VND

        // dữ liệu đầu vào là USD
        Scanner sc3 = new Scanner(System.in);
        System.out.print("Nhập USD: ");
        double usd = sc3.nextDouble();

        // quá trình xử lý : đổi USD sang VND với 1 USD = 23.500đ

        if (usd >= 0){
            double vnd = usd * 23.500;
            // xuất kết quả
            System.out.println(usd + "USD" +  " = " + vnd + "VND");
        }else {
            System.out.println("Số không hợp lệ");
        }

        // TODO Đổi độ C sang độ F
        Scanner sc4 = new Scanner(System.in);
        System.out.print("Nhập độ C: ");
        double doCnguoiDungNhap = sc4.nextDouble();
        double doiSangDoF = (doCnguoiDungNhap * 1.8) + 32;
        System.out.println(doCnguoiDungNhap + "độ C = " + doiSangDoF + "độ F");

        // TODO tính biểu thức P(8) = a * 8^n
        Scanner sc5 = new Scanner(System.in);
        System.out.print("Nhập a: ");
        float soA = sc5.nextFloat();
        System.out.print("Nhập n: ");
        int soN = sc5.nextInt();
        if (soN > 0){
            float ketQuaBai5 = (float) (soA * Math.pow(8, soN));
            System.out.println("P(8)= " + ketQuaBai5);
        }else {
            System.out.print("N phải là số nguyên dương");
        }
        
        //TODO Tính trung bình 5 số người dùng nhập 
        double ketQuaBai6 = 0;
        Scanner sc6 = new Scanner(System.in);
        System.out.print("Nhập số thứ 1: ");
        ketQuaBai6 += sc6.nextDouble();
        System.out.print("Nhập số thứ 2: ");
        ketQuaBai6 += sc6.nextDouble();
        System.out.print("Nhập số thứ 3: ");
        ketQuaBai6 += sc6.nextDouble();
        System.out.print("Nhập số thứ 4: ");
        ketQuaBai6 += sc6.nextDouble();
        System.out.print("Nhập số thứ 5: ");
        ketQuaBai6 += sc6.nextDouble();
        ketQuaBai6 = ketQuaBai6 / 5;
        System.out.println("Trung bình 5 số là:" + ketQuaBai6);
        







    }
}